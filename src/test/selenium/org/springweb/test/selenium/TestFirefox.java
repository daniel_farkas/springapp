package org.springweb.test.selenium;

import static org.junit.Assert.assertEquals;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class TestFirefox {
	
	private static WebDriver driver;
	
	@FindBy(xpath="//div[@class='jumbotron']/p")
	private WebElement labelformulario;
	
	public TestFirefox(){
		PageFactory.initElements(driver, this);
	}
	
	@BeforeClass
	public static void afterClass(){
		
		driver = new FirefoxDriver();
		driver.get("http://localhost:8080/springapp");
	}
	
	@Test
	public void teste(){
		
		assertEquals(labelformulario.getText(), "Formulário");
		
	}
	
	
	
	@AfterClass
	public static void beforeClass(){
		
		driver.close();
		driver.quit();
	}

}
