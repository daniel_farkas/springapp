package org.springweb.model.converters;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springweb.model.entities.Veiculo;
import org.springweb.scan.services.IHomeService;

public class VeiculoConverter implements Converter<String, Veiculo> {

	@Override
	public Veiculo convert(String pId) {
		
		Long id = Long.parseLong(pId);
		
		List<Veiculo> veiculos = homeService.getVeiculos();
		
		for(Veiculo veiculo : veiculos){
			if(veiculo.getId().equals(id)){
				return veiculo;
			}
		}
		
		return null;
	}

	@Autowired
	private IHomeService homeService;
}
