package org.springweb.model.entities;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "veiculos")
@AttributeOverride(name = "id", column = @Column(name = "vei_id"))
public class Veiculo extends AbstractEntity {

	@Column(name = "vei_modelo")
	private String modelo;

	@ManyToOne
	@JoinColumn(name = "usu_id")
	private Usuario usuario;
	
	public Veiculo(){}
	
	public Veiculo(Long pId, String pModelo, Usuario pUsuario){
		
		setId(pId);
		setModelo(pModelo);
		setUsuario(pUsuario);
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

}
