package org.springweb.scan.persistence.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springweb.model.entities.Veiculo;

@Repository
public class VeiculoDAO {
	
	@PersistenceContext
	EntityManager em;
	
	public List<Veiculo> getAll(){
		return em.createQuery("from Veiculo v", Veiculo.class).getResultList();
	}

}
