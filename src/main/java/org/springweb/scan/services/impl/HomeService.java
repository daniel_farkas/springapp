package org.springweb.scan.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springweb.model.entities.Usuario;
import org.springweb.model.entities.Veiculo;
import org.springweb.scan.persistence.dao.UsuarioDAO;
import org.springweb.scan.persistence.dao.VeiculoDAO;
import org.springweb.scan.services.IHomeService;

@Service
public class HomeService implements IHomeService {


	@Override
	public List<Usuario> getUsuarios() {
		return usuarioDAO.getAll();
	}

	@Override	
	public void inserirUsuario(Usuario pUsuario) {
		usuarioDAO.inserir(pUsuario);
	}
	
	@Override
	public void getUsuarioById(Long pId) {
		Usuario usuario = usuarioDAO.getById(pId);
		usuario.setNome("CARAJAS");
	}

	@Override
	public List<Veiculo> getVeiculos() {		
		return veiculoDAO.getAll();
	}
	
	@Autowired
	private UsuarioDAO usuarioDAO;
	
	@Autowired
	private VeiculoDAO veiculoDAO;
}
