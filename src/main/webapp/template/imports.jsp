<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<!-- CSS -->
<link href="${pageContext.request.contextPath}/css/jquery-ui.css" type="text/css" rel="stylesheet"/>
<link href="${pageContext.request.contextPath}/css/jquery.ui.datepicker.css" type="text/css" rel="stylesheet"/>
<link href="${pageContext.request.contextPath}/css/bootstrap.css" type="text/css" rel="stylesheet"/>
<link href="${pageContext.request.contextPath}/css/bootstrap-theme.css" type="text/css" rel="stylesheet"/>
<link href="${pageContext.request.contextPath}/css/estilo.css" type="text/css" rel="stylesheet"/>
<!-- JS -->
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery/jquery-2.1.0.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery/jquery-ui.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery/jquery.ui.widget.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery/jquery.maskedinput.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery/jquery.validate.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery/jquery.ui.datepicker.js"></script>
<c:if test="${lang == 'pt_BR'}">
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery/i18n/jquery.ui.datepicker-pt_BR.js"></script>
</c:if>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/bootstrap/bootstrap.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/app.js"></script>